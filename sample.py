#!/usr/bin/env python3
# -*- coding: ascii -*-

from golem import *
from golem.desc import db, lang, framework

from golem.attack import Attack
from golem.attack.sqlinjection import SQLInjectionAtk
from golem.attack.accesscontrol import *

from golem.crawler import FormCrawler
from golem.talker import Response

@lang(lang.PYTHON)
@db(db.MYSQL)
@backend(framework.DJANGO)
@frondend(framework.ANGULARJS)
class TestApp(WebApp):

    @attack(SQLInjectionAtk)
    @api('/polls/new')
    def test_sqli_on_polls_new(self, attack):
        crawler = FormCrawler()
        forms = crawler.get_forms()

        for form in forms:
            fields = form.fields

            for field in fields:
                attack.require(field).run()


    @attack(AccessControlAtk)
    def test_access_control(self, attack):
        ac = AccessControl()

        admin = AccessItem(User('admin'))
        admin.set_valid_resp(Response.OK)
        admin.set_invalid_resp(Response.NOT_FOUND)
        admin.add_valid_api(AccessItem.ALL_APIS)

        user = AccessItem(User('user'))
        user.set_valid_resp(Response.OK)
        user.set_invalid_resp(Response.NOT_FOUND)
        user.add_valid_api(['/polls/', '/polls/1/'])
        user.add_invalid_api(['/polls/new/', '/polls/1/update/'])

        ac.add([admin, user])
        attack.require(ac).run()
